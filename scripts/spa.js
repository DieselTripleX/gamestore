function SPA(iframeDOMElement) {
  this.redirect = function(pageAddress) {
    console.log('Redirect to ' + pageAddress)
    iframeDOMElement.src = pageAddress;
    setTimeout(function () {
      window.location.hash = pageAddress;
    }, 200)
  }

  var subpage = window.location.href.split('#')[1];
  if (subpage) {
    this.redirect(subpage)
  } else {
    this.redirect('pages/main.html')
  }
}

var spa = new SPA(document.getElementById('content'));
