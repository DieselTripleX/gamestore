function Menu(menuDOMElement) {
  var menuItems = menuDOMElement.children;

  function removeActiveClass() {
    for (let i = 0; i < menuItems.length; i++) {
      const item = menuItems[i];
      item.classList.remove('active')
    }
  }

  for (let i = 0; i < menuItems.length; i++) {
    const item = menuItems[i];
    item.addEventListener('click', function() {
      if (item.className.includes('disabled')) return

      removeActiveClass();
      spa.redirect(item.dataset.url);
      item.classList.add('active');
    });

    if ('#' + item.dataset.url == window.location.hash) {
      removeActiveClass();
      item.classList.add('active');
    }
  }
}

var menu = new Menu(document.getElementById('playstation-buttons'))
var zhopa = new Menu(document.getElementById('logo'))

