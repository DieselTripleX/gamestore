function MultiItemSlider(selector) {
  var currentItemIndex = 0;

  var wrapper = document.querySelector(selector + ' .slider-wrapper');
  var maxIndex = document.querySelectorAll(selector + ' .slider-item').length - 1;

  var controls = {
    left: document.querySelectorAll(selector + ' .slider-button-left'),
    right: document.querySelectorAll(selector + ' .slider-button-right'),
  };

  function renderWrapper(index) {
    wrapper.style.transform = 'translateX(' + (-100 * index) +'%)';
  }

  function render() {
    renderWrapper(currentItemIndex);
  }

  for (i = 0; i < controls.left.length; i++) {
    controls.left[i].addEventListener('click', function() {
      if (currentItemIndex == 0) return;
      currentItemIndex -= 1;
      render();
    });

    controls.right[i].addEventListener('click', function() {
      if (currentItemIndex == maxIndex) return;
      currentItemIndex += 1;
      render();
    });
  }

  render();
}

var multiItemSlider = new MultiItemSlider('#games');
